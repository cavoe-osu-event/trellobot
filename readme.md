# Trellobot
A simple Discord bot to log and report events from your Trello boards in your Discord server.
Originally forked from [Angush/trellobot](https://github.com/Angush/trellobot),
but uses webhooks instead, to fix errors with the API sending to much data.


![Image example of Trellobot alert](https://raw.githubusercontent.com/Eierpflanze/trellobot/master/example-alert.png "Image example of Trellobot alert")

## Setup
1. Clone repository.
2. Run `npm install`.
3. Configure `conf.json` file as desired ([see below](#confjson)).
4. Generate tokens and set up `.auth` file ([see below](#auth)).
5. All done. Run Trellobot with `node trellobot.js`.

## conf.json
There are several important values in here which inform Trellobot's operation. Here's what they're all for, and how to set them.

*(optional properties marked with * asterisks)*

Property         | Explanation
---------------- | -----------
`boardMapping`   | Mapping of board IDs to channels determining on which boards Trellobot reports and to which channel it sends messages regarding that board. IDs can be extracted from the URLs of your Trello boards. (eg. the board ID for [https://trello.com/b/**HF8XAoZd**/welcome-board](https://trello.com/b/HF8XAoZd/welcome-board) is `HF8XAoZd`). For channel IDs enable developer mode in Discord and right click a channel to copy its ID.
`serverID`       | An ID string determining which Discord server Trellobot uses. Enable developer mode in Discord and right click a server icon to copy its ID.
`prefix`*        | A string determining the prefix for Trellobot commands in Discord. Currently unused. Defaults to `.` (period).
`contentString`* | A string included posted alongside all embeds. If you'd like to ping a certain role every time the bot posts, for example, you would put that string here.
`enabledEvents`* | An array of event names (strings) determining whitelisted events (ie. which events will be reported; if empty, all events are enabled). Eligible event names can be found [in the `events.md` file](https://github.com/Eierpflanze/trellobot/blob/master/events.md).
`userIDs`*       | An object mapping Discord IDs to Trello usernames, like so: `userIDs: {"TrelloUser": "1395184357104955", ...}`, so Trellobot can pull relevant user data from Discord.
`realNames`*     | A boolean (defaulting to true) that determines whether Trellobot uses the full names or usernames from Trello (eg. `John Smith` vs `jsmiff2`)
`callbackUrl`*   | The URL the server will use to send webhook callbacks to

You can refer to the `conf.json` included in the repository for an example.

## .auth
The `.auth` file is included as a template to save you time, but you will need to create the keys and tokens yourself to run Trellobot. Here's how:

Property       | How to get the value
-------------- | ----------------------
`discordToken` | Create an app for Trellobot to work through on [Discord's developer site](https://discordapp.com/developers/applications/me/create), then create a bot user (below app description/icon) and copy the token.
`trelloKey`    | Visit [this page](https://trello.com/1/appKey/generate) to generate your public Trello API key.
`trelloToken`  | Visit `https://trello.com/1/connect?name=Trellobot&response_type=token&expiration=never&key=YOURPUBLICKEY` (replacing `YOURPUBLICKEY` with the appropriate key) to generate a token that does not expire. Remove `&expiration=never` from the URL if you'd prefer a temporary token.