var request = require("request"),
  EventEmitter = require("events").EventEmitter,
  http = require("http"),
  extend = require("extend"),
  config,
  e;

var boardIdMapping = {};
e = new EventEmitter();

module.exports = function(options) {
  var defaults = {
    trello: {
      key: "",
      token: "",
      boards: [],
      callbackURL: ""
      // boards: ['WQtuArbW', ...]
    },
    start: true
  };

  console.log("Starting bot for boards")
  config = extend(true, defaults, options);

  for (const board of defaults.trello.boards) {
    addWebhook(board);
  }

  var server = http.createServer();
  var port = process.env.PORT || 8080;

  server
    .on("request", function(req, res) {
      if (req.method === "HEAD") {
        res.statusCode = 200;
        res.end();
      } else if (req.method === "POST") {
        let body = [];
        req.on('data', (chunk) => {
          body.push(chunk);
        }).on('end', () => {
          body = Buffer.concat(body).toString();
          handleWebhook(body, res);
        });
      } else {
        res.statusCode = 403;
        res.end();
      }
    })
    .listen(port);

  var self = {
    on: function(event, listener) {
      e.on(event, listener);
      return self;
    }
  };

  return self;
};

function addBoardIdToMapping(boardId) {


}

function handleWebhook(body, res) {
  var webhook = JSON.parse(body);
  e.emit(
    webhook.action.type,
    webhook.action,
    boardIdMapping[webhook.action.data.board.id]
  );
  res.statusCode = 200;
  res.end();
}

function getBoardModelIdByBoardId(boardId) {
  return Object.keys(boardIdMapping).find(key => boardIdMapping[key] === boardId);
}

function addWebhook(boardId){

  //First get the modelID for later use
  var options = {
    method: "GET",
    url: "https://api.trello.com/1/boards/" + boardId ,
    qs: {
      key: config.trello.key,
      token: config.trello.token
    }
  };

  request(options, function(error, response, body) {
    if (error) {
      console.log(error);
    }

    if(response.statusCode === 200){
      var id = JSON.parse(body).id;
      boardIdMapping[id] = boardId;
      idAPICallCallback(id);
    } else {
      console.log("Model id could not be found");
      console.log(response.statusCode + ' > ' + body);
    }
  });

}

//After we are done getting the id we can generate the webhook
function idAPICallCallback(modelId){

  var options = {
    method: "POST",
    url:
        "https://api.trello.com/1/webhooks/?idModel="
        + modelId
        + "&description=\"Webhook for board " + boardIdMapping[modelId]
        + "\"&callbackURL=" + config.trello.callbackURL,
    qs: {
      key: config.trello.key,
      token: config.trello.token
    }
  };

  request(options, function(error, response, body) {
    if (error) {
      console.log(error);
    }
    if(response.statusCode === 200){
      console.log("Webhook for board " + boardIdMapping[modelId] + " created successfully");
    } else {
      console.log("Webhook creation failed");
      console.log(response.statusCode + ' > ' + body);
    }
  });

}
